$(document).ready(function() {
	
	//
	var ww = document.body.clientWidth;
	//var wh = $('body').height();
	var wh = document.body.clientHeight;
	var hh = $('#header').outerHeight();
	$(document).ready(function() {		
		$(".nav li a").each(function() {
			if ($(this).next().length > 0) {
				$(this).parent('li').addClass("parent");
			};
		});
		adjustMenu();
	});
	$(window).bind('resize orientationchange', function() {
		ww = document.body.clientWidth;
		wh = $('body').height();
		hh = $('#header').outerHeight();
		adjustMenu();
	});
	var adjustMenu = function() {
		if(ww < 1025) {			
			$(".nav li").on('click',  function(){
                $(this).siblings('li').removeClass('open');
                $(this).toggleClass('open');
      		});
		}
		else if(ww >= 1025) {
			$('.mobile-button').removeClass('open');
			$('.nav').removeClass('open');
			$('#header').removeClass('open');
			$('.js-btn-filter').removeClass('open');
			$('.side-filts').removeClass('open');
			$('.side-block__title.js-opener').removeClass('open');
			$('.side-block__drop').removeClass('open');
			$('.js-btn-filter').text("фильтры");

			$(".nav li").hover(function() {
                $(this).addClass('open');
                $(this).siblings('li').addClass('dis');
            },
            function() {
                $(this).removeClass('open');
                $(this).siblings('li').removeClass('dis');
            });

		}
		$('.slide-link').height(wh-hh);
		$('.vh').height(wh);

		function showDiv() {
			if ($(window).scrollTop() > hh && $('.js-fix-top').data('positioned') == 'false') {
				$(".js-fix-top").data('positioned', 'true');
				$(".js-fix-top").addClass('fix');
			}else if ($(window).scrollTop() <= hh && $('.js-fix-top').data('positioned') == 'true') {
				$(".js-fix-top").fadeIn(0, function() {
					$(".js-fix-top").removeClass('fix');
				}).data('positioned', 'false');
			}
		}
		$(window).scroll(showDiv);
		$(window).load(showDiv);
		$('.js-fix-top').data('positioned', 'false');

	}

	//
	$('.animated').appear(function() {
		var element = $(this);
		var animation = element.data('animation');
		var animationDelay = element.data('delay');
		if (animationDelay) {
		  setTimeout(function(){
			element.addClass( animation + " visible" );
			element.removeClass('hiding');

		  }, animationDelay);
		}else {
		  element.addClass( animation + " visible" );
		  element.removeClass('hiding');

		}    
	},{accY: -50});

	//slider-inline
    $('.slide-link').each(function(e){
 		$(this).css('background-image', 'url('+$(this).find("img").attr("src")+')');
 	});	
	$(window).load(function() {
		$('video').each(function () {
	        this.play();
	        this.muted = true;
	    });
		if ($('.covervid-video').length) {
			    $('.covervid-video').coverVid(1920, 1080);
		}	    
	});
	$('.img-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        fade: false,
        dots: false,
        asNavFor: '.content-tabs',
        speed: 500
    });
    $('.content-tabs').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.img-slider',
		dots: false,
		arrows: false,
        fade: true,
        adaptiveHeight: true,
		focusOnSelect: true
	});
    $('.content-tabs').on('afterChange', function(event, slick, currentSlide, nextSlide){
		$('.content-tabs .slick-current').find('.animated').removeClass('visible').removeClass('fadeInUp').addClass('hiding');
		
		$('.animated').appear(function() {
			var element = $(this);
			var animation = element.data('animation');
			var animationDelay = element.data('delay');
			if (animationDelay) {
			  setTimeout(function(){
				element.addClass( animation + " visible" );
				element.removeClass('hiding');

			  }, animationDelay);
			}else {
			  element.addClass( animation + " visible" );
			  element.removeClass('hiding');

			}    
		},{accY: -50});
	});



	//fix-header
	function showDiv() {
		if ($(window).scrollTop() > 160 && $('#header').data('positioned') == 'false') {
			$("#header").data('positioned', 'true');
			$("#header").addClass('fix');
		}else if ($(window).scrollTop() <= 160 && $('#header').data('positioned') == 'true') {
			$("#header").fadeIn(0, function() {
				$("#header").removeClass('fix');
			}).data('positioned', 'false');
		}
	}
	$(window).scroll(showDiv);
	$(window).load(showDiv);
	$('#header').data('positioned', 'false');

	//placeholder
	$('[placeholder]').focus(function() {
		var input = $(this);
		if(input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if(input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if(input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

	//
	$('.mobile-button').click(function() {
		$(this).toggleClass('open');
		$('.nav').toggleClass('open');
		$('#header').toggleClass('open');
		return false;
	});

    //
    $(".link-down").click(function() {
		$("html, body").animate({ scrollTop: $('#home-block1').offset().top -160}, "700");
		return false;
	});

    // 
	$('.tov-img-item').addClass('hid');
	$('.tov-img-item:first-child').removeClass('hid');
	$('.tov-img-slider li:first-child').addClass('current');
	$(document).on('click', '.tov-img-slider li a', function() {		
		var idx = $(this).parent().index();
		$(this).parents('.tov-img-block').find('.tov-img-item').addClass('hid');
		$(this).parents('.tov-img-block').find('.tov-img-item').eq(idx).removeClass('hid');		
		$('.tov-img-slider li').not($(this).parent('li')).removeClass('current');
		$(this).parent('li').addClass('current');
		return false;
	});

	//
	$(window).scroll(function(){
		if ($(this).scrollTop() > 700) {
			$('.link-up').fadeIn();
		} else {
			$('.link-up').fadeOut();
		}
	});
	$(".link-up").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "700");
		return false;
	});

	//js-video
	$('.js-video').click(function(){
		$(this).parent('.video-cov').removeClass(' cover'); 
		$('#zollavideo')[0].play();
	    return false;
	});

	//
	var $svg = $('.letter svg').drawsvg();
	$('.letter.in-view svg').drawsvg('animate').addClass('done');

	$(window).scroll(function(){
	    $('.letter.in-view svg').not('.done').drawsvg('animate').addClass('done');
	});

	//
	$.fn.moveIt = function(){
		var $window = $(window);
		var instances = [];
	  
		$(this).each(function(){
			instances.push(new moveItItem($(this)));
		});
	  
		window.onscroll = function(){
		    var scrollTop = $window.scrollTop();
		    instances.forEach(function(inst){
		    	inst.update(scrollTop);
		    });
		}
	}
	var moveItItem = function(el){
		this.el = $(el);
		this.speed = parseInt(this.el.attr('data-scroll-speed'));
	};
	moveItItem.prototype.update = function(scrollTop){
		var pos = (scrollTop-$window.height()) / this.speed;
		this.el.css('margin-top',  -(pos) );
	};
	// Initialization
	$(function(){
		$('[data-scroll-speed]').moveIt();
	});

	//
	$('select, input[type="radio"], input[type="checkbox"]').styler();

	//
	$('.js-btn-filter').click(function() {
		$(this).toggleClass('open');
		$('.side-filts').toggleClass('open');
		var text = $(this).text();
		$(this).text(text == "скрыть" ? "фильтры" : "скрыть");
		return false;
	});

	$('.side-block__title.js-opener').click(function() {
		$(this).toggleClass('open');
		$(this).next('.side-block__drop').toggleClass('open');
		return false;
	});
	$('.search-button').click(function() {
		$(this).parent('.head-search').toggleClass('open');
		$('.head-search .t-inp').focus();
		return false;  
	});
	$(document).mouseup(function(e) {
        var container = $(".head-search");
        if (!container.is(e.target) 
            &&
            container.has(e.target).length === 0)        {
            $(".head-search").removeClass('open');
        }
    });

	//tabs
	$('.pane:first-child').addClass('active');
	/*$('.filter-tabs li:first-child').addClass('current');									- закоментить для программиста
	$('.filter-tabs li a').click(function() {		
		var idx = $(this).parent().index();
		$(this).parents('.center').find('.pane').not($('.pane').eq(idx)).removeClass('active');
		$(this).parents('.center').find('.pane').eq(idx).addClass('active');
		$('.filter-tabs li').not($(this).parent('li')).removeClass('current');
		$(this).parent('li').addClass('current');
		return false;
	});*/
	//
	$('.win-panes pane:first-child').addClass('active');
	$('.win-tabs li:first-child').addClass('current');
	$('.win-tabs li a').click(function() {		
		var idx = $(this).parent().index();
		$(this).parents('.win').find('.pane').not($('.pane').eq(idx)).removeClass('active');
		$(this).parents('.win').find('.pane').eq(idx).addClass('active');
		$('.win-tabs li').not($(this).parent('li')).removeClass('current');
		$(this).parent('li').addClass('current');
		return false;
	});


	//reset                																	- закоментить для программиста
	/*$('.reset').click(function(){
		$(this).parent('.side-block__drop').find('.check-block .jq-checkbox').removeClass('checked'); 
	    return false;
	});*/

	$('.tov .head-fav').click(function(){
		$(this).toggleClass('selected'); 
	    return false;
	});

	

	//range                																	- закоментить для программиста
	$("#filter-slider").slider({
		min: 0,
		max: 5000,
		values: [0,5000],
		range: true,
		stop: function(event, ui) {
			jQuery("input#minCost").val(jQuery("#filter-slider").slider("values",0));
			jQuery("input#maxCost").val(jQuery("#filter-slider").slider("values",1));
		},
		slide: function(event, ui){
			jQuery("input#minCost").val(jQuery("#filter-slider").slider("values",0));
			jQuery("input#maxCost").val(jQuery("#filter-slider").slider("values",1));
		}
	});

	//big-new-bg
    $('.new-bg').each(function(e){
 		$(this).css('background-image', 'url('+$(this).next("img").attr("src")+')');
 	});

 	$('.faq-line-top').click(function() {
		$(this).toggleClass('open');
		$(this).next('.faq-line-drop').toggleClass('open');
		return false;
	});

	//
	$('.sidelist .js-active').click(function(){
		$(this).parent('ul').toggleClass('open'); 
	    return false;
	});
	$('.win .js-active').click(function(){
		$(this).toggleClass('open'); 
	    return false;
	});

    //card-img-slider
    $('.card-img-slider').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        infinite: false,
        arrows: false,
        fade: false,
        dots: true,
        speed: 500,
        responsive: [
		{
		  breakpoint: 1025,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
		  }
		}
	  ]
    });

	//
	$('.card-info-col').theiaStickySidebar({
    	additionalMarginTop: 102
    });

    //btn-dis
	$('.btn-dis').hover(function () {
	    $(this).text('выберите размер');
	}, function () {
	    $(this).text('НАЛИЧИЕ В МАГАЗИНЕ');
	});

	$('.rad').on('click', function(e) {
	    $('.btn-dis').off('mouseenter mouseenter');
	})

	if ($('.fancy').length) {
	    $('.fancy').fancybox();
	}
	if ($('.fancy-img').length) {
	    $('.fancy-img').fancybox({
	    	wrapCSS: 'fan-c',
	    	//maxWidth  : 9999,
	   	});
	}
    
    $(".tov-fav a").click(function () {
		$(this).toggleClass('selected');
		$(this).text(function(i, text){
			return text === "Добавить в избранное" ? "в избранном" : "Добавить в избранное";
		});
		return false;
	});

	//
	$('.tov-acc-opener').on('click', function(e) {
	    $(this).parent().toggleClass('open');
	    $(this).next().slideToggle(300);
	})
	
	//
	$('.tiles a').each(function(e){
 		$(this).css('background-image', 'url('+$(this).find("img").attr("src")+')');
 	});
	$('.news-page-tiles').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        infinite: false,
        arrows: false,
        fade: false,
        dots: true,
        speed: 500,
        responsive: [
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
		  }
		}
	  ]
    });

    //
    $(".js-link-info").click(function() {
		$("html, body").animate({ scrollTop: $('#card-info-col').offset().top -59 }, "slow");
		return false;
	});

	//
	$('.tabs-block .pane').addClass('hid');
	$('.tabs-block .pane:first-child').removeClass('hid');
	$('.tabs-block ul.tabs li a').click(function() {
	  var idx = $(this).parent().index();
	  $('.tabs-block .pane').not($('.tabs-block .pane').eq(idx)).addClass('hid');
	  $('.tabs-block .pane').eq(idx).removeClass('hid');
	  $('.tabs-block ul.tabs li').removeClass('current');
	  $(this).parent('li').addClass('current');
	  return false;
    });

    if ($('.v-scroll').length) {
	    $('.v-scroll').mCustomScrollbar({
	        axis:"y",
	        scrollButtons:{enable:false},
	        advanced:{autoExpandHorizontalScroll:true},
	        scrollInertia: 0,
	        scrollbarPosition:"inside"
	    });
	}

	//
	var timer, el = $('body'),
        flag = false;
    $(window).scroll(function() {
        if (!flag) {
            flag = true;
            el.addClass('is-scrolling');
        }
        clearTimeout(timer);
        timer = setTimeout(function() {
            el.removeClass('is-scrolling');
            flag = false;
        }, 200);
    });

    //
    /*var slideSelector = 'img',
    options = {
    	showHideOpacity: true,
        history: false,
        bgOpacity: 0,
        loop: false,
        maxSpreadZoom: 2.5,
        getThumbBoundsFn : function () {
            var imgThumb = data.event && data.event.target;

            if (!imgThumb) {
                return;
            }

            var pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                rect = imgThumb.getBoundingClientRect();

            return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
        }
    },
    events = {
        initialZoomInEnd: function () {
            var zoomLevel = 0;
            if (ww >= 1025) {
                zoomLevel = 1;
            } else if (ww < 1025) {
                zoomLevel = 1;
            } else {
                zoomLevel = 1;
            }
            setTimeout(function () {
                pswp.zoomTo(3, {
                    x: pswp.viewportSize.x / 2,
                    y: pswp.viewportSize.y / 2
                }, 222);
            }, 222);
            if (ww >= 1025) {
                $('img.pswp__img').on('tap', function () {
                    pswp.close();
                });
            }
        }
    };
    var pswp = $('#gallery').photoSwipe(slideSelector, options, PhotoSwipeUI_Default);
*/
    
    var pswp = $('#gallery').photoSwipe('img',{
    	showHideOpacity: true,
        history: false,
        bgOpacity: 0,
        loop: false,
        maxSpreadZoom: 2.5
    },
    {
    	getThumbBoundsFn : function () {
            var imgThumb = data.event && data.event.target;
            if (!imgThumb) {
                return;
            }
            var pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                rect = imgThumb.getBoundingClientRect();
            return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
        },
    	initialZoomInEnd: function () {
            var zoomLevel = 0;
            if (ww >= 1025) {
                zoomLevel = 1;
            } else if (ww < 1025) {
                zoomLevel = 1;
            } else {
                zoomLevel = 1;
            }
            setTimeout(function () {
                /*pswp.zoomTo(3, {
                    x: pswp.viewportSize.x / 2,
                    y: pswp.viewportSize.y / 2
                }, 222);*/
            }, 222);
            if (ww >= 1025) {
                $('img.pswp__img').on('tap', function () {
                    //pswp.close();
                });
            }
        }
    });


});
